#pragma once

#include <iostream>
#include <boost/array.hpp>
#include <boost/asio.hpp>

using boost::asio::ip::tcp;

class NotConnectedException : public std::exception
{
public:
	NotConnectedException();
	NotConnectedException(const char* message);
	virtual ~NotConnectedException() throw();
};

class SimpleClient
{
public:
	SimpleClient();
	~SimpleClient();
	bool Connect(std::string hostname, std::string port);
	void Run();

private:
	void ProcessServerResponse();

	tcp::iostream* _stream;
	bool _connected;
};

